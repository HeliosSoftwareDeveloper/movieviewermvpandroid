package com.heliossoftwaredeveloper.movieviewer.Utilities;

/**
 * Created by rngrajo on 30/01/2018.
 */

public class Constant {

    public static final String API_URL_GET_MOVIES = "http://cayaco.info/movielist/list_movies_page1.json";
    public static final String URL_IMAGE_LINK_COVER = "http://cayaco.info/movielist/images/[SLUG]-cover.jpg";
    public static final String URL_IMAGE_LINK_BACKDROP = "http://cayaco.info/movielist/images/[SLUG]-backdrop.jpg";

    public static final String TOOLBAR_MOVIELIST = "Movie List";
    public static final String TOOLBAR_EMPTY = "";
    public static final int API_MODE_GET_MOVIES = 3000;
}
